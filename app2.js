const express = require('express');
//const bodyParser = require('body-parser')

const app = express();//Antes de la 4.16 se utiliza bodyParser
app.use(express.urlencoded());
app.use(express.json());

app.get('/',function(req,res){
    res.send('API de productos v1.0')
});

app.get('/producto', function(req,res){
    var dato = {id:1, nombre:'impresora', precio:500, stock:200}
    res.send(dato)
});

app.post('/producto', function(req,res){
    mens = {mensaje: 'producto creado correctamente'};
    console.log(req.body);
    if(!req.body.id || !req.body.nombre){
        mens = {mensaje:'Error, se requiere el id y el nombre'};
    }
    //Aqui va el codigo para grabar en la BD
    res.status(201);
    res.send(mens)
})
app.put('/producto', function(req,res){
    console.log(req.body);
    res.send('Actualizado ok')
})
app.listen(8001,()=>{
    console.log('El servidor esta inicializado en el puerto 8001')
});